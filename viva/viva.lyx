#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass beamer
\begin_preamble
\usetheme[darktitle]{UniversityOfManchester}
% or ...

\setbeamercovered{transparent}
% or whatever (possibly just delete it)
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures false
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
This file is a solution template for:
\end_layout

\begin_layout Itemize
Talk at a conference/colloquium.
 
\end_layout

\begin_layout Itemize
Talk length is about 20min.
 
\end_layout

\begin_layout Itemize
Style is ornate.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
 
\end_layout

\begin_layout Plain Layout
In principle, this file can be redistributed and/or modified under the terms
 of the GNU Public License, version 2.
 However, this file is supposed to be a template to be modified for your
 own needs.
 For this reason, if you use this file as a template and not specifically
 distribute it as part of a another package/program, the author grants the
 extra permission to freely copy and modify this file as you see fit and
 even to delete this copyright notice.
 
\end_layout

\end_inset


\end_layout

\begin_layout Title

\lang british
Towards High Performance Metal Detector With Interference Immunity Through
 System Modelling and Optimization
\end_layout

\begin_layout Author
Qiaoye
\begin_inset space ~
\end_inset

Ran
\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
If you have a file called "institution-logo-filename.xxx", where xxx is a
 graphic format that can be processed by latex or pdflatex, resp., then you
 can add a logo by uncommenting the following:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%
\backslash
pgfdeclareimage[height=0.5cm]{institution-logo}{institution-logo-filename}
\end_layout

\begin_layout Plain Layout

%
\backslash
logo{
\backslash
pgfuseimage{institution-logo}}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
The following causes the table of contents to be shown at the beginning
 of every subsection.
 Delete this, if you do not want it.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
AtBeginSubsection[]{%
\end_layout

\begin_layout Plain Layout

  
\backslash
frame<beamer>{ 
\end_layout

\begin_layout Plain Layout

    
\backslash
frametitle{Outline}   
\end_layout

\begin_layout Plain Layout

    
\backslash
tableofcontents[currentsection,currentsubsection] 
\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
If you wish to uncover everything in a step-wise fashion, uncomment the
 following command:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%
\backslash
beamerdefaultoverlayspecification{<+->}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Outline
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Structuring a talk is a difficult task and the following structure may not
 be suitable.
 Here are some rules that apply for this solution: 
\end_layout

\begin_layout Itemize
Exactly two or three sections (other than the summary).
 
\end_layout

\begin_layout Itemize
At *most* three subsections per section.
 
\end_layout

\begin_layout Itemize
Talk about 30s to 2min per frame.
 So there should be between about 15 and 30 frames, all told.
\end_layout

\begin_layout Itemize
A conference audience is likely to know very little of what you are going
 to talk about.
 So *simplify*! 
\end_layout

\begin_layout Itemize
In a 20min talk, getting the main ideas across is hard enough.
 Leave out details, even if it means being less precise than you think necessary.
 
\end_layout

\begin_layout Itemize
If you omit details that are vital to the proof/implementation, just say
 so once.
 Everybody will be happy with that.
 
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Introduction
\end_layout

\begin_layout Subsection
Basics of Metal Detector
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
A Typical Metal Detector:
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Consists of a transmitter that generates a primary electromagnetic field
 and a receiver that measures the induced fields caused by interactions
 of the primary field from the environment
\end_layout

\begin_layout Itemize
Takes full use of the metallic property (conductivity and permeability)
 for subsurface discrimination
\end_layout

\begin_layout Itemize
Is widely exploited in different cases: such as landmine detection, metal
 contaminants detection in food and pharmaceutical industry (in-line metal
 detector) and public safety for potential threat objects
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
In-line Metal Detector
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout ColumnsCenterAligned

\end_layout

\begin_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\begin_layout Block

\end_layout

\begin_layout Block
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/introduction/inlinesystem.svg
	width 100col%

\end_inset


\end_layout

\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Basic Function
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
The whole system relies on the conveyor belt to deliver the target products
 through the center of the metal detector at a certain speed.
\end_layout

\begin_layout Itemize
The rejecter will remove the defective ones from the belt once metal contaminant
s are detected.
\end_layout

\end_deeper
\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
In-line Metal Detector
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout ColumnsCenterAligned

\end_layout

\begin_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Balancing System
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Consist of a middle transmitting coil and two adjacent coaxial receiving
 coils at a same geometrical distance
\end_layout

\begin_layout Itemize
When an electric signal is applied to the transmitting coil, the differential
 output voltage between the two receiving coils is captured
\end_layout

\end_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\begin_layout Block

\end_layout

\begin_layout Block
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/introduction/metaldetector.svg
	width 100col%

\end_inset


\end_layout

\end_deeper
\end_deeper
\begin_layout Subsection
Research Motivation
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Research Motivation 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
Its design and performance is strictly regulated by corresponding standards
 and highly driven by customer demand.
\end_layout

\begin_layout Enumerate
In factory, a large number of devices bring along with a problem of electromagne
tic noise and interference.
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Block
This research aims at enhancing the performance of the detectors in terms
 of the interference immunity
\end_layout

\end_deeper
\begin_layout Section
Research and Results
\end_layout

\begin_layout Subsection
Multiple Receiving Coil Proposal
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Multiple Receiving Coil Proposal
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
It has two pairs of receiving coils for sensing the signals.
\end_layout

\begin_layout Itemize
Both sets of receiver coils will obtain the product signal and the noise
 from the environment.
\end_layout

\begin_layout Itemize
Invert the output signal of the secondary receivers and add to the output
 signal of the primary receivers to get a better SNR.
\end_layout

\begin_layout Itemize
Multiple a fixed coefficient with output signal of the secondary receivers.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Multiple Receiving Coil Proposal
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/multiplecoilarray/twosetofcoils.svg
	width 100col%

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Measuring the coefficient
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
It is estimated from the difference of two amplitudes of the output signals.
\end_layout

\begin_layout Itemize
The resultant coefficient will determine the coil array design.
\end_layout

\begin_layout Itemize
Quantify the outside interference on both sets of receiving coils.
\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Two possible methods:
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
Magnetic Field (B field) Analysis
\end_layout

\begin_layout Enumerate
Mutual Inductance Analysis
\end_layout

\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Magnetic Field (B field) Analysis
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
From Faraday’s law of induction, any change in magnetic flux through an
 adjacent circuit could induce an electromotive force across that circuit.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\varepsilon=V=-\frac{d\phi}{dt}
\]

\end_inset


\end_layout

\begin_layout Itemize
Corresponding to the magnetic field around the receiving coil and area of
 it.
\end_layout

\begin_layout Itemize
The shape of the coil is fixed, the interference could be seen as the magnetic
 field induced from the circuit.
\end_layout

\begin_layout Itemize
The magnitude of its surrounding 
\begin_inset Formula $B$
\end_inset

 field is used to quantify the possible interference at different locations.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Magnetic Field Calculation
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout ColumnsTopAligned

\end_layout

\begin_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout ColumnsTopAligned
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/multiplecoilarray/Rectangularmagneticfield.svg
	width 100col%

\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Formula
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Derived from the classic Biot-Savart law:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
B=\frac{\mu_{0}I}{4\pi}\oint_{l}\frac{d\overrightarrow{L}\times\overrightarrow{I_{r}}}{\overrightarrow{R}^{2}}
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
B filed at any observation point P:
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
\begin_inset Formula $B_{x}=\frac{\mu_{0}Iz}{4\pi}\varoint\frac{-1}{R^{3}}dy_{0}$
\end_inset


\end_layout

\begin_layout Standard
on Line 1 and 3
\end_layout

\begin_layout Itemize
\begin_inset Formula $B_{y}=\frac{\mu_{0}Iz}{4\pi}\varoint\frac{1}{R^{3}}dx_{0}$
\end_inset


\end_layout

\begin_layout Standard
on Line 2 and 4
\end_layout

\begin_layout Itemize
\begin_inset Formula $B_{z}=\frac{\mu_{0}I(a-x)}{4\pi}\varoint\frac{-1}{R^{3}}dy_{0}+\frac{\mu_{0}I(b-y)}{4\pi}\varoint\frac{1}{R^{3}}dx_{0}+\frac{\mu_{0}I(a+x)}{4\pi}\varoint\frac{1}{R^{3}}dy_{0}+\frac{\mu_{0}I(-b-y)}{4\pi}\varoint\frac{1}{R^{3}}dx_{0}$
\end_inset


\end_layout

\begin_layout Standard
on Line 1, 2, 3 and 4
\end_layout

\end_deeper
\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Mutual Inductance Analysis
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
The relationship between the induced electromotive force and the change
 of the current of the interference source:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
V=-M\cdot\frac{di}{dt}
\]

\end_inset


\end_layout

\begin_layout Itemize
The other method is to calculate the mutual inductance between two coils
 (receiving coil and interference coil).
\end_layout

\begin_layout Itemize
Two coaxial rectangular receiving coils would be analyzed, one localized
 outside the metal detector as the interference.
\end_layout

\begin_layout Itemize
Ignore the turns and cross-section area of the coils.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Mutual Inductance Calculation
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/multiplecoilarray/Rectangularcoil_mutual.svg
	width 50col%

\end_inset


\end_layout

\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Derived from the classic Neumann’s Formula:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M=(\frac{\mu_{0}}{4\pi})\ointop_{C_{1}}\ointop_{C_{2}}\frac{d\overrightarrow{l_{1}}\cdot d\overrightarrow{l_{2}}}{R}
\]

\end_inset


\end_layout

\begin_layout Standard
Mutual Inductance between two coaxial rectangular coils:
\end_layout

\begin_layout Standard
\begin_inset Formula $M=(\frac{\mu_{0}}{4\pi})\ointop_{C_{1}}\ointop_{C_{2}}\frac{(dx_{1},dy_{1},dz_{1})\cdot(dx_{2},dy_{2},dz_{2})}{R}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $R=\sqrt{\left[x_{1}-(Tx+X')\right]^{2}+\left[y_{1}-(Ty+Y')\right]^{2}+(z_{1}-Tz)^{2}}$
\end_inset


\end_layout

\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Discussion
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout ColumnsTopAligned

\end_layout

\begin_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/multiplecoilarray/Coefficient_formula_Bfield.svg
	width 100col%

\end_inset


\end_layout

\end_deeper
\begin_layout ColumnsTopAligned
\begin_inset Formula $Coefficient=\frac{B_{1}}{B_{2}}$
\end_inset


\end_layout

\begin_layout ColumnsTopAligned
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/multiplecoilarray/Mutual_reciever.svg
	width 100col%

\end_inset


\end_layout

\begin_layout ColumnsTopAligned
\begin_inset Formula $Coefficient=\frac{M_{1}-M_{2}}{M_{3}-M_{4}}$
\end_inset


\end_layout

\begin_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout ColumnsTopAligned
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/multiplecoilarray/equation_coefficient.svg
	lyxscale 50
	width 100col%

\end_inset


\end_layout

\begin_layout ColumnsTopAligned

\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Conclusion
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Both approaches and their corresponding equations are capable of measuring
 the magnitude of the interference.
\end_layout

\begin_layout Itemize
The magnetic field analysis is more simple to implement, since it can obtain
 the required coefficient on 3-D space.
\end_layout

\begin_layout Itemize
A FEM based solver is required to do some simulations with the case.
\end_layout

\begin_layout Itemize
The simulation model of mutual inductance analysis is more complicated and
 results are much noisy.
\end_layout

\end_deeper
\begin_layout Subsection
Coil Array Design
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Coil Array Design
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout ColumnsTopAligned

\end_layout

\begin_deeper
\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
The coil array determines the magnetic sensitivity of a metal detector,
 which is influenced by the coil size and coil pitch (CP).
\end_layout

\begin_layout Itemize
The sensitivity is proportional to the dot product of two magnetic field
 strength, HT and HR.
\end_layout

\begin_layout Column
\begin_inset ERT
status open

\begin_layout Plain Layout

.5
\backslash
columnwidth
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout ColumnsTopAligned
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/coilarraydesign/V5.svg
	width 100col%

\end_inset


\end_layout

\begin_layout ColumnsTopAligned
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/coilarraydesign/alldatasensitivity_V5.svg
	width 100col%

\end_inset


\end_layout

\begin_layout ColumnsTopAligned

\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout PlainFrame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Optimized Coil Pitch Identification
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Definition
\begin_inset Formula $Sensitivity_{CP1}-Sensitivity_{CP2}\times Coefficient_{CP1/CP2}$
\end_inset


\end_layout

\begin_layout Itemize
It represents the sensitivity at two different locations, CP1 and CP2
\end_layout

\begin_layout Itemize
The coefficient is selected through the weighted average value within a
 3-D space outside the metal detector (8x6x6m, with a step of 0.1m)
\end_layout

\begin_layout Definition
\begin_inset Formula $ScaledSensitivity=\frac{OutputSensitivity}{\beta}$
\end_inset


\end_layout

\begin_layout Definition
\begin_inset Formula $\beta=1+Coefficient_{CP1/CP2}$
\end_inset


\end_layout

\begin_layout Itemize
The scaled sensitivity is obtained by considering the SNR.
\end_layout

\begin_layout Itemize
The calculated sensitivity should be scaled by its noise level.
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Optimized Coil Pitch Identification
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/coilarraydesign/penalisedsensitivity_V5.svg
	width 80col%

\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Optimized Coil Pitch Identification
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename D:/SourceTree/firstyearreport/figure/coilarraydesign/sensitivity_subtraction_V5.svg
	width 85col%

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Future Plan
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Future Plan
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Build a whole active noise control system
\end_layout

\begin_deeper
\begin_layout Itemize
Coil Array design (finished)
\end_layout

\begin_layout Itemize
Tuning circuit (finished)
\end_layout

\begin_layout Itemize
Adaptive Filter application
\end_layout

\begin_layout Itemize
Test real-time signals with interference
\end_layout

\begin_layout Itemize
Other algorithms (eg.
 Kalman Filter)
\end_layout

\end_deeper
\begin_layout Itemize
Electronics design
\end_layout

\begin_layout Itemize
Flange design (possible)
\end_layout

\end_deeper
\end_body
\end_document
