#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass uomreport
\begin_preamble
\usepackage{graphicx}
\usepackage{url} % typeset URL's reasonably
\usepackage{listings}
\usepackage{pslatex} % Use Postscript fonts
\end_preamble
\options 12pt,PhD
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style british
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Conclusions and Future Work
\begin_inset CommandInset label
LatexCommand label
name "chap:Conclusions-and-Future"

\end_inset


\end_layout

\begin_layout Section
Conclusions
\end_layout

\begin_layout Standard
This progress report aims at addressing the current issues, which limit
 the performance of in-line metal detector in interference immunity through
 system modelling and optimization.
 To achieve this objective, multiple receivers design was proposed based
 on the in-line metal detector.
 Magnetic field analysis and mutual inductance analysis were both theoretically
 explained by the Biot-Savart law and Neumann’s Formula in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Multiple-Coil-Array"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 The derived equations and calculated coefficient maps on 
\begin_inset Formula $XY$
\end_inset

, 
\begin_inset Formula $XZ$
\end_inset

 and 
\begin_inset Formula $YZ$
\end_inset

 planes verified the previous assumptions and briefly described the coefficient
 distribution without the effect of the case.
 The standard metal detector and SEFA metal detector models were then studied
 through the FEM simulations in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Coil-Array-Design"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 The optimized CP combination for both sets of receiving coils was determined
 by comparing the peak scaled sensitivity map after obtaining the average
 weighted coefficient for all the possible CPs.
 The simulations showed that this interference immunity technique could
 eliminate the influence of external noise sources on the received signal
 to some extent, however, the sensitivity to the metal contaminants was
 also suppressed.
\end_layout

\begin_layout Section
Future Work
\end_layout

\begin_layout Standard
The research on the novel coil array design demonstrates the benefits and
 inadequacies of this proposal based upon the derived numerical formulas
 and a number of simulations in FEM solver 
\begin_inset Formula $AnsysMaxwell^{\mathbb{\circledR}}$
\end_inset

.
 However, these conclusions should be then tested by in situ measurements
 in real-world environment on a SEFA metal detector with multiple sets of
 receiving coils.
 By using this solver, the optimized coil pitch combination is only tested
 for a few detectors, so more models are required to be studied to instruct
 the development of coil arrays.
\end_layout

\begin_layout Standard
Except for the hardware design, data acquisition and processing methods
 is considered as the next step.
 Compared with these passive noise control methods, active noise control
 (ANC) is more efficient over a wide range of frequencies, especially at
 lower frequencies 
\begin_inset CommandInset citation
LatexCommand cite
key "george2013advances"
literal "false"

\end_inset

.
 The ANC system usually consists of an error sensor to measure the level
 of environment noise and achieves noise cancellation in terms of the residual
 noise 
\begin_inset Formula $e(n)$
\end_inset

, along with the reconstruction of sensor design and circuit.
 In addition, some advanced algorithms is going to be exploited and applied
 in it.
\end_layout

\begin_layout Standard
Electronics design and flange design are another alternative solutions for
 noise suppression.
 In electronics analysis, all the cabling, grounding and passive components,
 including capacitors, inductors, resistors and etc.
 could be the possible noise source.
 The flange is more related to the metal free zone rule, providing an ideal
 electromagnetic environment to function properly.
 A detailed future plan is listed in the following Gantt Chat.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/conclusion/Ganttchart.svg
	width 50col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Gantt Chat
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
