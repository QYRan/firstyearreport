#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass uomreport
\begin_preamble
\usepackage{graphicx}
\usepackage{url} % typeset URL's reasonably
\usepackage{listings}
\usepackage{pslatex} % Use Postscript fonts
\end_preamble
\options 12pt,PhD
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style british
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Introduction
\end_layout

\begin_layout Section
Research Motivation
\end_layout

\begin_layout Standard
Metal detection is of great importance in a range of modern industries.
 In-line metal detector is widely employed in food and pharmaceutical manufactur
ing to separate the metal contaminants from the product, of which design
 and performance is strictly regulated by corresponding standards and highly
 driven by customer demand.
 The production of metal detectors in different cases is a mature technology
 already, however, there are still many places for in-depth research and
 improvement.
 In factory, simultaneous usage of a large number of devices brings along
 with it the problem of electromagnetic noise and interference, which degrades
 the quality of received signal for metal detectors.
 This progress report aims at enhancing the performance of the detectors
 in terms of the interference immunity by proposing a novel design criteria
 based on the current balanced coil system.
\end_layout

\begin_layout Section
Basics of In-line Metal Detector
\end_layout

\begin_layout Standard
A whole in-line metal detector consists of a conveyor, a metal detector
 and a rejecter, shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:In-line-metal-detector"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 When the system is working properly, it relies on the conveyor belt to
 deliver the target products through the centre of the metal detector at
 a certain speed for inspection and the rejecter will remove the defective
 ones from the belt once metal contaminants are detected.
 The metal detector includes a stainless steel case, an electrostatic screen
 and a balanced coil system, shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Metal-Detector"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 The case shields the inside coils from the outside interference, providing
 an environment to function satisfactorily.
 The electrostatic screen is used to reduce the capacitive coupling between
 the product and the coils, resulting in a small product signal.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/introduction/inlinesystem.svg
	width 60col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
In-line Metal Detector System 
\begin_inset CommandInset label
LatexCommand label
name "fig:In-line-metal-detector"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/introduction/metaldetector.svg
	width 75col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Metal Detector 
\begin_inset CommandInset label
LatexCommand label
name "fig:Metal-Detector"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
The balanced coil system are wrapped around the case, basically consisting
 of a middle transmitting coil and two adjacent coaxial receiving coils.
 The transmitting coil is powered by an alternative current source and generates
 a primary magnetic field once excitation.
 The two receiving coils are opposite wired, normally distributed on both
 sides of the transmitting coil at a same geometrical distance, which are
 able to receive the magnetic filed and convert back to a voltage.
 Theoretically, when an electric signal is applied to the transmitting coil,
 the differential output voltage between the two receiving coils is zero
 with the signal subtraction.
 Therefore, there is no background signal and the system is balanced.
\end_layout

\begin_layout Standard
Once a metal contaminant is introduced in the transmitting primary magnetic
 field, according to the Faraday's law of induction and Lenz’s law, eddy
 currents will be induced in planes perpendicular to the magnetic field
 on the surface of the metal, which can create a secondary magnetic field
 that opposes the change in the primary field.
 The eddy currents and resultant skin depth mainly depend on the exciting
 frequency and the conductivity and permeability of the metal.
 The receiving coil can capture such disturbance of magnetic field in its
 region.
 If for example, the object passes through one receiver, the magnetic field
 in this coil will change due to the eddy currents, in comparison, the other
 receiver cannot synchronize an identical change in magnetic field, which
 means the output voltage cannot be zero any more.
 Such voltage change will alert the system if metal contaminants are detected.
\end_layout

\begin_layout Standard
When the received signal is obtained, an amplifying circuit is applied at
 the beginning and a low pass filter is connected.
 Then the signal is transmitted to a phase sensitive detector (PSD) and
 demodulated into the real and imaginary parts.
 Next, the resulting real and imaginary components are converted into digital
 signals respectively and ready for further signal processing.
 It is mentioned that the digital signal processing normally consists of
 a low pass filter for high frequency noise, a high pass filter for background
 DC noise and match filters for signal classification and identification.
 A simple equivalent circuit is shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Equivalent-Circuit"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement h
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/introduction/blockdiagram.svg
	width 90col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Equivalent Circuit
\begin_inset CommandInset label
LatexCommand label
name "fig:Equivalent-Circuit"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Conclusions
\end_layout

\begin_layout Standard
This progress report begins with a board background within the in-line metal
 detector research, including some necessary hardware design for suppressing
 the electromagnetic interference which has been successfully applied.
 Then, a novel coil array design consisting of two sets of receiving coils
 is proposed, which aims at enhancing the performance of metal detector
 in interference immunity.
 The supporting theories and derived formulas are displayed in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Multiple-Coil-Array"
plural "false"
caps "true"
noprefix "false"

\end_inset

 and the resultant simulation results are shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Coil-Array-Design"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 Furthermore, future plan on both hardware and software improvements is
 listed in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Conclusions-and-Future"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 In addition, 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Alternative-Application-Hand-Ges"
plural "false"
caps "true"
noprefix "false"

\end_inset

 describes a planned hand gesture recognition system as the extra work in
 this report, which relies upon the electromagnetic induction (EMI) sensors
 to capture the hand tracking and deep learning algorithms for signal processing.
\end_layout

\end_body
\end_document
