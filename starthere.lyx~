#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass uomreport
\begin_preamble
\usepackage{graphicx}
\usepackage{url} % typeset URL's reasonably
\usepackage{listings}
\usepackage{pslatex} % Use Postscript fonts
\end_preamble
\options 12pt,PhD
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style british
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Towards High Performance Metal Detector With Interference Immunity Through
 System Modelling And Optimization
\end_layout

\begin_layout Author
Qiaoye Ran
\begin_inset Newline newline
\end_inset

10078027
\end_layout

\begin_layout School
Electrical and Electronic Engineering
\end_layout

\begin_layout Faculty
Science and Engineering
\end_layout

\begin_layout Preface Section
Abstract
\end_layout

\begin_layout Standard
Metal detection technique is extensively utilized in the food and pharmaceutical
 industries for preventing potential metal contaminants within the targeted
 products.
 The design of in-line metal detector bases upon rich theoretical foundation
 and a large number of experimental verification.
 In industry, its key features are already described in corresponding guidance
 and the performance is strictly regulated by the industrial standards and
 legislations.
 However, further research is required on different issues.
 This report provides a novel coil array design on the interference immunity,
 inspired by the classic Biot-Savart law and Neumann’s Formula and achieved
 by the FEM simulations in 
\begin_inset Formula $AnsysMaxwell^{\mathbb{\circledR}}$
\end_inset

.
 This technique still requires more experiments by in situ measurements
 as the future research.
\end_layout

\begin_layout End of Abstract

\end_layout

\begin_layout Preface Section
Acknowledgements
\end_layout

\begin_layout Standard
I would like to thank to my supervisor, Dr Wuliang Yin and Prof Anthony
 Peyton for all their helpful instructions and comments in this research.
 I am also grateful to Mr.
 Daren Butterworth, Dr Christos Ktistis, Dr Yang Tao, Dr Yifei Zhao in Safeline
 Ltd and all my colleagues at the University of Manchester for their help
 and support.
\end_layout

\begin_layout Standard
I would like to thank the Mettler Toledo Safeline Ltd (UK) for the financial
 support in this year at the University of Manchester.
\end_layout

\begin_layout Standard
My special thanks give to my parents and my family, who are always supporting
 and encouraging me during the past year.
\end_layout

\begin_layout End of Preface

\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "introduction.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "background.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "multiplecoilarray.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "coilarraydesign.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "conclusion.lyx"

\end_inset


\end_layout

\begin_layout Start of Appendix

\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "handgesture.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
btprint "btPrintCited"
bibfiles "reference/ProgressReport"
options "IEEEtran"

\end_inset


\end_layout

\end_body
\end_document
