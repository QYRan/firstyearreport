#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass uomreport
\begin_preamble
\usepackage{graphicx}
\usepackage{url} % typeset URL's reasonably
\usepackage{listings}
\usepackage{pslatex} % Use Postscript fonts
\end_preamble
\options 12pt,PhD
\use_default_options true
\maintain_unincluded_children false
\language british
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style british
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Coil Array Design
\begin_inset CommandInset label
LatexCommand label
name "chap:Coil-Array-Design"

\end_inset


\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
The coil array determines the magnetic sensitivity of a metal detector,
 which is influenced by the coil size and coil pitch (CP).
 In the design of an in-line metal detector, as the coil size is regulated
 for the production, the optimized CP for the coil array is of great importance.
 Theoretically, the optimized CP for different symmetrical coil arrays are
 analysed in free space, such as common rectangular and circular coil arrays
 
\begin_inset CommandInset citation
LatexCommand cite
key "brighton1993calculation"
literal "true"

\end_inset

.
 In industry, due to the case effect, the optimized CP needs to be re-determined
 by analysing the sensitivity distribution along the central observation
 line.
\end_layout

\begin_layout Standard
In this chapter, the optimized CP for the standard model (Aperture size:
 
\begin_inset Formula $350\times175mm$
\end_inset

) with the novel multiple receiving coil design will be discussed, shown
 in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Standard-In-line-Metal"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 Considering the complexity of the case size and its effect to the magnetic
 field, we propose to solve the model by running the simulations in a FEM
 based software, 
\begin_inset Formula $AnsysMaxwell^{\mathbb{\circledR}}$
\end_inset

at a relatively high frequency 
\begin_inset Formula $850$
\end_inset

 
\begin_inset Formula $kHZ$
\end_inset

.
 The simulation results could accurately indicate the sensitivity change
 and calculate the magnetic field at any observation points in three dimensions.
 Therefore, the optimized CP can be obtained by the resulting sensitivity
 with the effect of the measured coefficient.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/V5.svg
	width 100col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Standard In-line Metal Detector
\begin_inset CommandInset label
LatexCommand label
name "fig:Standard-In-line-Metal"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Sensitivity Analysis
\end_layout

\begin_layout Standard
As mentioned in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "sec:Electromagnetic-Sensitivity"
plural "false"
caps "true"
noprefix "false"

\end_inset

, for an in-line metal detector, the sensitivity is proportional to the
 dot product of two magnetic field strength, 
\begin_inset Formula $H_{T}$
\end_inset

 and 
\begin_inset Formula $H_{R}$
\end_inset

, which represents the magnetic field induced only by the currents in transmitti
ng coil and receiving coils respectively.
 For a standard in-line metal detector with only one set of receiving coils,
 the sensitivity change along the central line through the coil array is
 similar to one completed cycle of a sinusoidal wave, with two equal but
 opposite peaks located close to each of the receiving coils.
 The amplitude and position of the peak sensitivity are influenced by the
 CP and the magnetic sensitivity for different CP, varied from 
\begin_inset Formula $10mm$
\end_inset

 to 
\begin_inset Formula $210mm$
\end_inset

, observed at the central line is shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Sensitivity-along-the"
plural "false"
caps "true"
noprefix "false"

\end_inset

 and the peak sensitivity for each CP is plotted in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Peak-Sensitivity-for"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/alldatasensitivity_V5.svg
	width 75col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Sensitivity along the Central Line
\begin_inset CommandInset label
LatexCommand label
name "fig:Sensitivity-along-the"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/peaksensitivity.svg
	width 75col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Peak Sensitivity for different CP
\begin_inset CommandInset label
LatexCommand label
name "fig:Peak-Sensitivity-for"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
It can be seen that the magnetic sensitivity increases with the increment
 of CP and decreases as CP reaches to 
\begin_inset Formula $105mm$
\end_inset

.
 In industry, the ratios between the optimized CP and the coil aperture
 height are generally regulated for the production.
 However, for a metal detector with multiple receiving coils, the optimized
 CP cannot comply with such regulation.
\end_layout

\begin_layout Section
Optimized Coil Pitch Identification
\begin_inset CommandInset label
LatexCommand label
name "sec:Optimized-Coil-Pitch"

\end_inset


\end_layout

\begin_layout Standard
For different CP, the corresponding magnetic field is also obtained in 
\begin_inset Formula $AnsysMaxwell^{\mathbb{\circledR}}$
\end_inset

.
 Considering the 3-D information of the magnetic field 
\begin_inset Formula $B$
\end_inset

, the magnitude value of 
\begin_inset Formula $B$
\end_inset

 (Seen in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "eq:Bmagnitude"
plural "false"
caps "true"
noprefix "false"

\end_inset

) is applied for the coefficient analysis.
 The equation to calculate the coefficient is also mentioned in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "eq:coefficicient_B_fied"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 In order to have an overall analysis of a certain region around the metal
 detector, the selected 3-D space is 
\begin_inset Formula $8\times6\times6m$
\end_inset

, with a step of 
\begin_inset Formula $0.1m$
\end_inset

.
 In the process of the calculation, the coefficient inside the metal detector
 is far beyond the value outside of it, so the region of the metal detector
 itself should be removed, with a size of 
\begin_inset Formula $1\times1\times1m$
\end_inset

.
 Then, there are over three hundred thousand points in total located around
 the metal detector and the weighted average value is considered as the
 fixed coefficient for each CP combination.
 The sensitivity for multiple receiving coils with the effect of the coefficient
 can be written as:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
Sensitivity=Sensitivity_{CP1}-Sensitivity_{CP2}\times Coefficient_{CP1/CP2}\label{eq:overall_sensitivity}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $Coefficient_{CP1/CP2}$
\end_inset

 represents the resultant coefficient for the combination of primary set
 of receiving coil at 
\begin_inset Formula $CP1$
\end_inset

 and secondary set of receiving coil at 
\begin_inset Formula $CP2$
\end_inset

.
 It is noted that once the sensitivity is multiplied by a ratio, the wideband
 noise will also change with a same multiple.
 The maximum sensitivity may be accompanied with a relatively high noise
 amplification.
 Therefore, the calculated sensitivity should be scaled by its noise level.
 The scaled sensitivity is calculated as:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
ScaledSensitivity=\frac{Sensitivity_{CP1}-Sensitivity_{CP2}\times Coefficient_{CP1/CP2}}{\beta}\label{eq:Scaled_sensitivity}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\beta=1+Coefficient_{CP1/CP2}\label{eq:amplifying_factor}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $\beta$
\end_inset

 is defined as the noise amplifying factor, consisting of the ratio of 
\begin_inset Formula $Sensitivity_{CP1}$
\end_inset

 and 
\begin_inset Formula $Sensitivity_{CP2}$
\end_inset

.
 Hereafter, the scaled sensitivity map of the peak sensitivity value for
 all the possible combination is shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Scaled-Sensitivity-Map"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/penalisedsensitivity_V5.svg
	width 100col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Scaled Sensitivity Map
\begin_inset CommandInset label
LatexCommand label
name "fig:Scaled-Sensitivity-Map"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Considering the effect of the noise amplifying factor 
\begin_inset Formula $\beta$
\end_inset

, the scaled sensitivity map is symmetrical along the diagonal, where two
 coil pitches being equal.
 It is noted that the maximum peak sensitivity in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Scaled-Sensitivity-Map"
plural "false"
caps "true"
noprefix "false"

\end_inset

 reaches at the position 
\begin_inset Formula $(70mm,185mm)$
\end_inset

.
 Therefore, in this model, the optimized coil pitch combination is 
\begin_inset Formula $70mm$
\end_inset

 for the primary set of receiving coil and 
\begin_inset Formula $185mm$
\end_inset

 for the secondary set of receiving coil.
 The output sensitivity for this design through 
\begin_inset CommandInset ref
LatexCommand formatted
reference "eq:overall_sensitivity"
plural "false"
caps "true"
noprefix "false"

\end_inset

 is calculated in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Output-Sensitivity-for"
plural "false"
caps "true"
noprefix "false"

\end_inset

, of which peak value experiences a 
\begin_inset Formula $37.5\%$
\end_inset

 drop after subtraction and a 
\begin_inset Formula $47.8\%$
\end_inset

 drop compared with one receiving coil design (seen in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Peak-Sensitivity-for"
plural "false"
caps "true"
noprefix "false"

\end_inset

 at 
\begin_inset Formula $CP=105mm$
\end_inset

).
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/sensitivity_subtraction_V5.svg
	width 75col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Output Sensitivity for Multiple Coil Array
\begin_inset CommandInset label
LatexCommand label
name "fig:Output-Sensitivity-for"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Discussion
\end_layout

\begin_layout Standard
As the results found in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "sec:Optimized-Coil-Pitch"
plural "false"
caps "true"
noprefix "false"

\end_inset

, a model based on the optimized CP combination 
\begin_inset Formula $(70mm,185mm)$
\end_inset

 is built for the magnetic field discussion.
 Three different planes (
\begin_inset Formula $XY$
\end_inset

, 
\begin_inset Formula $XZ$
\end_inset

 and 
\begin_inset Formula $YZ$
\end_inset

) are selected as the observation plane.
 As common, the product conveyor is recognised as the global 
\begin_inset Formula $XY$
\end_inset

 plane and the origin of the coordinate locates at the centre of the metal
 detector, middle centre of the transmitting coil.
 Define the central line as the 
\begin_inset Formula $X$
\end_inset

 axis and the 
\begin_inset Formula $Z$
\end_inset

 axis points upwards from the plane following the right-hand rule.
 It is mentioned that the defined coordinate is different from that in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Multiple-Coil-Array"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 The 
\begin_inset Formula $YZ$
\end_inset

 plane is also selected at 
\begin_inset Formula $X=0.5m$
\end_inset

 instead of the middle, in order to obtain more relatively useful information.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/Banalysis_on_3D.svg
	width 100col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Magnetic Field Analysis on Different Planes
\begin_inset CommandInset label
LatexCommand label
name "fig:Magnetic-Field-Analysis"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Magnetic-Field-Analysis"
plural "false"
caps "true"
noprefix "false"

\end_inset

 shows the magnetic field (
\begin_inset Formula $B_{Magnitude}$
\end_inset

) of the primary set of receiving coils and coefficient (
\begin_inset Formula $B_{1}/B_{2}$
\end_inset

) distribution on three observation planes.
 As expected, the magnetic field gradually decreases as the absolute distance
 increases outside the case.
 In terms of the coefficient map, there are two distinct areas with different
 colours on each observation plane, symmetrically distributed at the origin,
 so for each plane, the coefficients are concentrated around two values.
 Furthermore, compared with the coefficient maps on the other two planes,
 the composition of two colour regions on 
\begin_inset Formula $XY$
\end_inset

 plane is more balanced, and the corresponding values are also much closer,
 less than 
\begin_inset Formula $10\%$
\end_inset

 difference around 
\begin_inset Formula $0.6$
\end_inset

.
 It can be seen that on 
\begin_inset Formula $XZ$
\end_inset

 and 
\begin_inset Formula $YZ$
\end_inset

 plane, the difference between the two coefficients is particularly large,
 about 
\begin_inset Formula $50\%$
\end_inset

, but the proportion of the larger coefficient is relatively small, so the
 weighted average value within the entire region is still stable at around
 
\begin_inset Formula $0.65$
\end_inset

.
 This can also be verified in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:-Magnitude-on-3Dspace"
plural "false"
caps "true"
noprefix "false"

\end_inset

, which plots the coefficient in the selected 3-D space.
 As mentioned, the selected space is 
\begin_inset Formula $8\times6\times6m$
\end_inset

, with a step of 
\begin_inset Formula $0.1m$
\end_inset

 and the metal detector itself (
\begin_inset Formula $1\times1\times1m$
\end_inset

) is removed.
 It also can be seen that the resultant coefficients in the entire space
 are concentrated in one region instead of being randomly distributed.
 Such simulation results also prove the accuracy and feasibility of the
 theory in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Multiple-Coil-Array"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/Bcoefficient_3D.svg
	width 80col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset Formula $B$
\end_inset

 Magnitude on 3-D space
\begin_inset CommandInset label
LatexCommand label
name "fig:-Magnitude-on-3Dspace"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Another Application
\end_layout

\begin_layout Standard
This approach is also applied in another metal detector model, named SEFA,
 with different CP combinations, varied from 
\begin_inset Formula $10mm$
\end_inset

 to 
\begin_inset Formula $235mm$
\end_inset

.
 The only difference between the two is the flange design inside the case,
 seen in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:SEFA-Metal-Detector"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 Repeat the previous experimental steps, the scaled sensitivity map of the
 peak sensitivity value from the simulations is shown in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Scaled-Sensitivity-Map-SEFA"
plural "false"
caps "true"
noprefix "false"

\end_inset

.
 The optimized CP combination is 
\begin_inset Formula $90mm$
\end_inset

 for the primary set of receiving coil and 
\begin_inset Formula $225mm$
\end_inset

 for the secondary set of receiving coil and its output sensitivity is plotted
 in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Output-Sensitivity-of-SEFA"
plural "false"
caps "true"
noprefix "false"

\end_inset

, of which peak value experiences a 
\begin_inset Formula $20.6\%$
\end_inset

 drop after subtraction.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/SEFA.svg
	width 100col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
SEFA Metal Detector
\begin_inset CommandInset label
LatexCommand label
name "fig:SEFA-Metal-Detector"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/penalisedsensitivity_SEFA.svg
	width 100col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Scaled Sensitivity Map of SEFA
\begin_inset CommandInset label
LatexCommand label
name "fig:Scaled-Sensitivity-Map-SEFA"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figure/coilarraydesign/sensitivity_subtraction_SEFA.svg
	width 75col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Output Sensitivity of SEFA
\begin_inset CommandInset label
LatexCommand label
name "fig:Output-Sensitivity-of-SEFA"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
It can be seen that the output sensitivity of SEFA model is much higher
 than that of the standard model.
 The flange design for SEFA model is also more convenient in production
 and installation.
 Therefore, the performance of a SEFA metal detector would be tested by
 in situ measurements for further research.
\end_layout

\begin_layout Section
Conclusion
\end_layout

\begin_layout Standard
The research on the coil array design is based on the peak sensitivity magnitude
 along the central line.
 Considering the case effect on the magnetic field, we instructed the simulation
s of two metal detectors for more detailed evaluations.
 For a conventional coil array design, the optimized CP is regulated for
 different shaped coils in production, but the design with multiple coil
 arrays is not able to comply with the rule.
 The approach mentioned in this chapter relies upon the magnetic field analysis
 within a selected 3-D space around the metal detector for different coil
 pitches.
 The weighted average coefficient is then applied in the calculation of
 the scaled sensitivity and the optimized CP for the multiple coil could
 be determined by the peak scaled sensitivity along the observation line.
 After subtraction of the signal between the two sets of receiving coils,
 the interference could be theoretically offset, however, the resultant
 sensitivity also has a large degree of decline.
 This approach has also been applied in some other models with different
 case design or aperture size in 
\begin_inset Formula $AnsysMaxwell^{\mathbb{\circledR}}$
\end_inset

.
 Its performance still requires more subsequent practical experiments to
 verify.
\end_layout

\end_body
\end_document
